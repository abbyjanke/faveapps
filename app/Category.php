<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Has appliances
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function appliances()
    {
      return $this->hasMany('App\Appliance');
    }

}
