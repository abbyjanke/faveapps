<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte\Client;
use App\Appliance;
use App\Category;
use App\Photo;
use Storage;

class GetAppliances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appliances:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get New and Updated Appliance Data from AppliancesDelivered.';

    /**
    * The collections to scrape from AppliancesDelivered.
    *
    * @var array
    */
    protected $collections = [
      'small-appliances',
      'dishwashers'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseURL = 'https://www.appliancesdelivered.ie/';
        $client = new Client();

        foreach($this->collections as $collection) {
          $category = Category::where('slug', $collection)->first();

          $crawler = $client->request('GET', $baseURL.$collection.'?sort=price_asc');

          // check for other pages

          $totalProducts = $crawler->filter('.search-results > div.search-results-header')->each(function($node) {
            return $node->filter('div.products-count')->text();
          });

          preg_match_all('!\d+!', trim($totalProducts[0]), $matches);
          $totalProducts = implode(' ', $matches[0]);

          $pages = (int)ceil($totalProducts/20);

          $i = 1;
          while($i <= $pages) {
            $this->filterProducts($baseURL.'&page='.$i, $client, $crawler, $category, $i);
              $i++;
          }
        }
    }

    private function filterProducts($baseURL, $client, $crawler, $category, $pageNum) {

      $this->info($category->slug.': page: '.$pageNum);

      $crawler->filter('.search-results > div.search-results-product')->each(function($node) use ($baseURL, $client, $crawler, $category) {
        $title = $node->filter('h4 > a')->text();
        $detailsURI = $node->filter('h4 > a')->attr('href');

        $detailsCrawler = $client->request('GET', $detailsURI);
        $details = $detailsCrawler->filter('.product-detail div.tab-pane')->each(function($tab) {
          if($tab->attr('id') == 'product-lg-overview') {
            $description = $tab->filter('p')->text();
            return $description;
          }
        });

        $description = null;

        foreach($details as $id => $detail) {
          if(!is_null($detail)) {
            $description = $detail;
          }
        }

        $appliance = new Appliance();
        $appliance->name = $title;
        $appliance->description = $description;
        $appliance->price = str_replace("€", "", $node->filter('h3.section-title')->text());
        $appliance->save();

        $appliance->categories()->attach($category);

        $images = $detailsCrawler->filter('.product-detail div.carousel-inner > div.item')->each(function($node) use ($appliance) {
          $imageURI = $node->filter('img')->attr('src');
          $contents = file_get_contents($imageURI);
          $name = md5(strtotime('now'));
          Storage::disk('public')->put($name, $contents);

          $photo = new Photo();
          $photo->appliance_id = $appliance->id;
          $photo->photo_name = $name;
          $photo->save();
        });
      });

    }
}
