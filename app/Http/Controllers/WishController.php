<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Appliance;
use Prologue\Alerts\Facades\Alert;

class WishController extends Controller
{

    public function index($slug = null, $filter = 'name') {
      $data['wishlist'] = false;

      if(is_null($slug)) {
        $user = Auth::user();
        $data['wishlist'] = true;
      } else {
        $user = User::findBySlug($slug);

        if($user->slug == Auth::user()->slug) {
          $data['wishlist'] = true;
        }
      }

      $data['pageTitle'] = $user->name."'s wishlist";
      $data['category'] = 'wishlist';

      if($filter == 'name') {
        $data['filter'] = 'Title';
        $sortCol = 'name';
        $sortOrder = 'asc';
      }

      if($filter == 'price_asc') {
        $data['filter'] = 'Price Low To High';
        $sortCol = 'price';
        $sortOrder = 'asc';
      }

      if($filter == 'price_desc') {
        $data['filter'] = 'Price High To Low';
        $sortCol = 'price';
        $sortOrder = 'desc';
      }

      $data['appliances'] = $user->wishlist()->orderBy($sortCol, $sortOrder)->paginate();

      return view('home', $data);
    }

    /**
     * Add an item to the current user's wishlist.
     *
     * @return redirect
     */
    public function add($slug)
    {
        $user = Auth::user();
        $appliance = Appliance::findBySlug($slug);

        // dummy proof make sure you can't add it twice.
        if(!$user->wishlist()->where('appliance_id', $appliance->id)->count()) {
          $user->wishlist()->attach($appliance);
          // check if it was attached
          if($user->wishlist()->where('appliance_id', $appliance->id)->count()) {
            Alert::add('success', "Successfully added {$appliance->name} to your wishlist.")->flash();
          } else {
            Alert::add('error', "Whoopsies our kittens are taking a nap and couldn't add {$appliance->name} to your wishlist.")->flash();
          }

        } else {
          Alert::add('info', 'That appliance already exists in your wishlist silly goose.')->flash();
        }

        return redirect()->back();
    }

    /**
     * Remove an item to the current user's wishlist.
     *
     * @return redirect
     */
    public function remove($slug)
    {
        $user = Auth::user();
        $appliance = Appliance::findBySlug($slug);

        $user->wishlist()->detach($appliance);

        // check if it was detached
        if($user->wishlist()->where('appliance_id', $appliance->id)->count()) {
          Alert::add('error', "Ruh Roh! something went wrong removing that item from your wishlist.")->flash();
        } else {
          Alert::add('warning', "Removed {$appliance->name} from your wishlist.")->flash();
        }


        return redirect()->back();
    }
}
