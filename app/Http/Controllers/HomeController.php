<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appliance;
use App\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category = 'browse', $filter = 'price_asc')
    {
        $data = [
          'category' => $category,
        ];

        $data['pageTitle'] = 'Browsing All Appliances';

        if($category !== 'browse') {
          $category = Category::where('slug', $category)->first();
          $data['pageTitle'] = 'Browsing '.$category->name;
        }

        if($filter == 'name') {
          $data['filter'] = 'Title';
          $sortCol = 'name';
          $sortOrder = 'asc';
        }

        if($filter == 'price_asc') {
          $data['filter'] = 'Price Low To High';
          $sortCol = 'price';
          $sortOrder = 'asc';
        }

        if($filter == 'price_desc') {
          $data['filter'] = 'Price High To Low';
          $sortCol = 'price';
          $sortOrder = 'desc';
        }

        $data['appliances'] = Appliance::category($category)->orderBy($sortCol, $sortOrder)->paginate(10);

        return view('home', $data);
    }

    public function show($slug)
    {
      $details['appliance'] = Appliance::where('slug', $slug)->first();
      $details['pageTitle'] = $details['appliance']->name.' Details';

      return view('appliance', $details);
    }
}
