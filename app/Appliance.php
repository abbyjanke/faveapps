<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Appliance extends Model
{

    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'price', 'description'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Belongs to categories.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
      return $this->belongsToMany('App\Category', 'appliance_categories');
    }

    /**
     * Has photographs
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photos()
    {
      return $this->hasMany('App\Photo');
    }

    public function scopeCategory($query, $category)
    {
      if($category == 'browse') {
        return $this->query;
      }

      $pivot = $this->categories()->getTable();

      return $query->whereHas('categories', function ($q) use ($category, $pivot) {
          $q->where("{$pivot}.category_id", $category->id);
      });
    }

    public function getPriceAttribute()
    {
      return '&euro; '. $this->attributes['price'];
    }
}
