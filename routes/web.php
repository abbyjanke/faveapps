<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('wishlist')->group(function () {
  Route::get('{slug?}', 'WishController@index')->name('wishlist');

  Route::middleware(['auth'])->group(function () {
      Route::get('{slug}/add', 'WishController@add')->name('wishlist.add');
      Route::get('{slug}/remove', 'WishController@remove')->name('wishlist.remove');
  });
});

Route::get('appliance/{slug}', 'HomeController@show')->name('appliance.details');

Route::get('{category?}/{filter?}', 'HomeController@index')->name('home');
