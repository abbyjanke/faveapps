# MyFavouriteAppliances.wow

*This is a development appliance and should not be used for a production environment.*

### Installation

1. Clone this repository.
2. Install all the necessary components with this command:

  ```
  composer install
  ```

3. Run the initial command to get all your entries.

  ```
  php artisan appliances:get
  ```

4. Set a Cron entry for running scheduled composer commands:

  ```
  * * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
  ```

  *This will automatically update your appliances everyday.*
