<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
          'dishwashers' => 'Dishwashers',
          'small-appliances' => 'Small Appliances'
        ];

        foreach($categories as $slug => $name) {
          $category = new Category();
          $category->name = $name;
          $category->slug = $slug;
          $category->save();
        }
    }
}
