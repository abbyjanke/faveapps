@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                  <div class="row">
                    {{ $pageTitle }}
                  </div>
                </div>

                <div class="card-body details">
                  <h4>{{ $appliance->name }}</h5>

                  <div class="row">
                    <div class="col-md-4 pull-left">
                      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <?php $i = 0; ?>
                          @foreach($appliance->photos()->get() as $photo)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}" @if($i == 0) class="active" @endif></li>
                            <?php $i++; ?>
                          @endforeach
                        </ol>
                        <div class="carousel-inner">
                          <?php $i = 0; ?>
                          @foreach($appliance->photos()->get() as $photo)
                            <div class="carousel-item @if($i == 0) active @endif">
                              <img class="d-block w-100" src="{{ asset($photo->photo_name) }}" alt="{{ $photo->photo_name }}">
                            </div>
                            <?php $i++; ?>
                          @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>

                      <strong class="price text-success">{!! $appliance->price !!}</strong>

                      @auth
                        @if(Auth::user()->wishlist()->where('appliance_id', $appliance->id)->count())
                          @if(isset($wishlist) && $wishlist == true)
                            <a href="{{ route('wishlist.remove', ['slug' => $appliance->slug]) }}" class="btn btn-danger btn-sm"><i class="fas fa-minus-circle"></i> Remove From Wishlist</a>
                          @else
                            <button class="btn btn-light btn-sm"><i class="fas fa-info-circle"></i> Already In Your Wishlist</button>
                          @endif
                        @else
                          <a href="{{ route('wishlist.add', ['slug' => $appliance->slug]) }}" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Add To Wishlist</a>
                        @endif
                      @endauth

                    </div>

                    <p class="pull-right col-md-8">{{ $appliance->description }}</p>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
