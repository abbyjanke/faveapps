@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-6">{{ $pageTitle }}</div>
                    <div class="col-md-6 text-right">
                      <div class="dropdown show">
                        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Sort By ({{ $filter }})
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('home', ['category' => $category, 'filter' => 'name'])}}">Name</a>
                          <a class="dropdown-item" href="{{ route('home', ['category' => $category, 'filter' => 'price_asc'])}}">Price Low To High</a>
                          <a class="dropdown-item" href="{{ route('home', ['category' => $category, 'filter' => 'price_desc'])}}">Price High To Low</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card-body">

                  <ul class="appliance-list list-unstyled">
                  @foreach($appliances as $appliance)
                    <li class="row">
                      <img src="{{ asset($appliance->photos()->first()->photo_name) }}" class="col-md-3" style="height: 150px; width:150px;" />
                      <div class="col-md-9">
                        <h5><a href="{{ route('appliance.details', ['slug' => $appliance->slug]) }}">{{ $appliance->name }}</a></h5>
                        <p>{{ str_limit(strip_tags($appliance->description), 250) }}</p>
                        <strong class="price text-success">{!! $appliance->price !!}</strong>
                        @auth
                          @if(Auth::user()->wishlist()->where('appliance_id', $appliance->id)->count())
                            @if(isset($wishlist) && $wishlist == true)
                              <a href="{{ route('wishlist.remove', ['slug' => $appliance->slug]) }}" class="btn btn-danger btn-sm float-right"><i class="fas fa-minus-circle"></i> Remove From Wishlist</a>
                            @else
                              <button class="btn btn-light btn-sm float-right"><i class="fas fa-info-circle"></i> Already In Your Wishlist</button>
                            @endif
                          @else
                            <a href="{{ route('wishlist.add', ['slug' => $appliance->slug]) }}" class="btn btn-primary btn-sm float-right"><i class="fas fa-plus-circle"></i> Add To Wishlist</a>
                          @endif
                        @endauth
                      </div>
                    </li>
                  @endforeach
                  </ul>

                  {{ $appliances->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
